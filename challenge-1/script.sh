#!/bin/bash

# Set the directory where the Nginx access logs are stored
LOG_DIR="./"

# Loop through all the Nginx access log files in the directory
for LOG_FILE in $(ls $LOG_DIR*access.log); do
  # Count the number of HTTP 500 errors in the log file
  ERROR_COUNT=$(grep -c 'HTTP/1.1" 500' $LOG_FILE)
  # Print the result
  echo "There were $ERROR_COUNT HTTP 500 errors in $LOG_FILE."
done

